
"use strict";
var path = require('path')
var Hapi = require('hapi');

// Create a new server
var server = new Hapi.Server();

// Setup the server with a host and port
server.connection({
    port: parseInt(process.env.PORT, 10) || 3000,
    host: '0.0.0.0',
    router: {
        stripTrailingSlash: true
    }
});




server.register([{
        register: require("./server/plugins/dal")
    },
    {
     register: require("./server/plugins/api")
    }
    ], function (err) {
        
        if(err){
            console.error('Error loading DAL', err);
        }else{
             
            var devices = server.plugins.dal.models.device;
            
            var repo = new server.plugins.dal.Repository(devices);
            
            // var d1 = repo.create({deviceType: 1, deviceUID: 'jkosdtre3453kker3'}, 1);
            // var d2 = repo.create({deviceType: 1, deviceUID: 'opo34543we435-934'}, 1);
            
            // d1.then(function(result){
            //      console.log(result);
            // });
            // d2.then(function(result){
            //      console.log(result);
            // });
            
            
            //  repo.getById(3,['deviceType']).then(function(d){
            //     d.deviceType = 1; 
                
            //     repo.save(d, 1).then(function(result){
                    
            //      console.log(result);
            //     });
            // }); 
            
            // repo.getMany({deviceType:1},['deviceUid']).then(function(result){
            //     console.log(result);
            // });
            
            // repo.getOne({created:{$gt :new Date(new Date().getTime() - 10*60000)}}).then(function(result){
            //     console.log(result);
            // });
            // repo.deleteById(2).then(function(result){
            //     console.log(result);
            // });
            // repo.delete({deviceType:1}).then(function(result){
            //     console.log(result);
            // });
        }
 });