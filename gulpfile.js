var Path = require('path');
var util = require('util');

var gulp           = require("gulp"),
    minifyHTML     = require("gulp-minify-html"),
    concat         = require("gulp-concat"),
    uglify         = require("gulp-uglify"),
    cssmin         = require("gulp-cssmin"),
    uncss          = require("gulp-uncss"),
    imagemin       = require("gulp-imagemin"),
    sourcemaps     = require("gulp-sourcemaps"),
    mainBowerFiles = require("main-bower-files"),
    inject         = require("gulp-inject"),
    less           = require("gulp-less"),
    filter         = require("gulp-filter"),
    glob           = require("glob"),
    wiredep        = require('wiredep').stream,
    livereload     = require('gulp-livereload'),
    nodemon        = require('gulp-nodemon'),
    notify         = require('gulp-notify');
    
    
var config = {
    paths: {
        html: {
            src:  "web/public/**/*.html",
            dest: "web/public"
        },
        javascript: {
            src:  ["web/public/js/src/**/*.js"],
            dest: "web/public/js"
        },
        css: {
            src: ["web/public/css/src/**/*.css"],
            dest: "web/public/css"
        },
        images: {
            src: ["web/public/images/**/*.jpg", "web/public/images/**/*.jpeg", "web/public/images/**/*.png"],
            dest: "web/public/images/"
        },
        less: {
            src: ["web/public/less/**/*.less", "!web/public/less/includes/**"],
            dest: "web/public/css"
        },
        bower: {
            src: "./bower_components",
            dest: "web/public/bower_components"
        },
        verbatim: {
            src: ["src/manifest.json", "src/favicon.png"],
            dest: "web/public"
        }
    }
};

gulp.task("html", function(){

     

        return gulp.src(config.paths.html.src)
            .pipe(inject(
                gulp.src(
                    mainBowerFiles(),
                    {read: false, cwd: "bower_components"}
                ),
                {name: "bower", addPrefix: '/bower_components'}
            )).pipe(inject(
                gulp.src(
                    [
                    './bower_components/**/build/**/*min.css',
                    './bower_components/**/dist/**/*min.css',
                    './bower_components/**/ui-grid.css']
                ),
                {name: "bower"}
            )).pipe(inject(gulp.src(config.paths.javascript.src), 
            {name: 'development',
            read: false,
            transform: function(filepath) {
                filepath = filepath.replace(/^\/web\/public/, '');
                return util.format('<script type="text/javascript" src="%s"></script>', filepath);
            }}))
            /*.pipe(minifyHTML())*/
            .pipe(gulp.dest(config.paths.html.dest));
});

gulp.task("scripts", function(){
    return gulp.src(config.paths.javascript.src)
        .pipe(sourcemaps.init())
        .pipe(concat("app.min.js"))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.paths.javascript.dest));
});

gulp.task("css", function(){
    return gulp.src(config.paths.css.src)
        .pipe(sourcemaps.init())
        .pipe(concat("theme.min.css"))
        .pipe(cssmin())
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest(config.paths.css.dest))
        .pipe(livereload());
});

gulp.task("images", function(){
    return gulp.src(config.paths.images.src)
        .pipe(imagemin({
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest(config.paths.images.dest));
});

gulp.task("bower_js", function(){
    return gulp.src(mainBowerFiles(), {base: "bower_components"})
        .pipe(gulp.dest(config.paths.bower.dest));
});

gulp.task("bower_css", function(){
    return gulp.src([
                    './bower_components/**/build/**/*min.css',
                    './bower_components/**/dist/**/*min.css'])
        .pipe(gulp.dest(config.paths.bower.dest));
});
gulp.task("less", function(){
    return gulp.src(config.paths.less.src)
        .pipe(sourcemaps.init())
        .pipe(less({
            paths: ["bower_components/bootstrap/less"]
        }))
        .pipe(uncss({
            html: glob.sync(config.paths.html.src),
        }))
        .pipe(concat("main.min.css"))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest(config.paths.css.dest))
        .pipe(filter("**/*.css"))
        .pipe(livereload());
});

gulp.task("verbatim", function(){
    gulp.src(config.paths.verbatim.src)
        .pipe(gulp.dest(config.paths.verbatim.dest));
});


gulp.task("build", ["bower_js", "bower_js", "html", "scripts", "css", "less", "images", "verbatim"]);

gulp.task("default", ["build"], function(){
     
    livereload.listen();
    gulp.watch(config.paths.html.src, ["html"]);
    gulp.watch(config.paths.javascript.src, ["scripts"]);
    gulp.watch(config.paths.bower.src, ["bower"]);
    gulp.watch(config.paths.images.src, ["images"]);
    gulp.watch(config.paths.verbatim.src, ["verbatim"]);

    gulp.watch(config.paths.css.src, ["css"]);
    gulp.watch(config.paths.less.src, ["less"]);
    
    
    nodemon({
		// the script to run the app
		script: 'index.js',
		ext: 'js'
	}).on('restart', function(){
		// when the app has restarted, run livereload.
		gulp.src('app.js')
			.pipe(livereload())
			.pipe(notify('Reloading page, please wait...'));
	})
});