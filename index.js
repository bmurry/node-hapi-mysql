"use strict";
var path = require('path')
var Hapi = require('hapi');
var Inert = require('inert');
var config = require(path.join(__dirname, 'server/config'));

// Create a new server
var server = new Hapi.Server();

// Setup the server with a host and port
server.connection({
    port:  config.application.port,
    host: config.application.host,
    router: {
        stripTrailingSlash: true
    }
});




module.exports = server;



server.register([{
        register: require('good'),
        options: {
            reporters: [{
                reporter: require('good-console'),
                events: { ops: '*', request: '*', log: '*', response: '*', 'error': '*' }
            }]
        }
    },{
        register: require('inert')
    }], function (err) {
		if(err){
			console.error('Failed loading community plugins', err);
		}else{
            server.register([{
                register: require(path.join(__dirname, 'server/plugins/dal'))
            },
            {
                register: require(path.join(__dirname, 'server/plugins/auth'))
            },
            {
                register: require(path.join(__dirname, 'server/plugins/bll'))
            },
            {
                register: require(path.join(__dirname, 'server/plugins/api'))
            },
            {
                register: require(path.join(__dirname, 'server/plugins/web'))
            }], function (err) {
                if(err){
					console.error('Failed loading application plugins', err);
				}else{
                    
                    if(config.db.seedDB){
                        
                        require(path.join(__dirname, 'server/seed.js'))(server);
                        
                    }
           
                    //Start the server
                    if (process.env.NODE_ENV !== 'test') {
                        server.start(function() {
                            //Log to the console the host and port info
                            console.log('Server started at: ' + server.info.uri);
                        });
                    }
				}
            });
		}
});

