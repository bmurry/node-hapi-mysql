"use strict";

module.exports = function() {

	var env = process.env.NODE_ENV || 'development';
	var host = host || '127.0.0.1';
	var port = port || 3030;
	var dbContants = databaseConfig();
	var appConstants = applicationConfig();
	
	
	console.info("CONFIG VERSION =   " + env);
	
	var obj = {
		application : {
			url : appConstants[env]['url'],
			host : appConstants[env]['host'],
			port : appConstants[env]['port'],
		},
		db : {
			host     : dbContants[env]['host'],
			user     : dbContants[env]['user'],
			password : dbContants[env]['password'],
			database : dbContants[env]['database'],
			dialect : dbContants[env]['dialect'],
			sync : dbContants[env]['sync'],
			forceSync : dbContants[env]['forceSync'],
			seedDB: dbContants[env]['seedDB']
		},
		server : {
			defaultHost : 'http://localhost:8001'
		}
	};

	if (!obj.application['host']) {
		throw new Error('Missing constant application.host. ' +
			'Check your enviroment variables NODE_HOST.');
	} else if (!obj.application['port']) {
		throw new Error('Missing constant application.port. ' +
			'Check your enviroment variable NODE_PORT.');
	} else if (!obj.db['host']) {
		throw new Error('Missing constant database.host. ' +
			'Check your enviroment variables.');
	} else if (!obj.db['user']) {
		throw new Error('Missing constant database.user. ' +
			'Check your enviroment variables.');
	} else if (!obj.db['password']) {
		throw new Error('Missing constant database.password. ' +
			'Check your enviroment variables.');
	} else if (!obj.db['database']) {
		throw new Error('Missing constant database.database. ' +
			'Check your enviroment variables.');
	} else if (!obj.db['dialect']) {
		throw new Error('Missing constant database.dialect. ' +
			'Check your enviroment variables.');
	}

	return obj;

	function databaseConfig(){
		return {
			'production' : {
				'host' : 'us-cdbr-iron-east-03.cleardb.net',
				'port' : 0,
				'user' : 'bdea843dac8a74',
				'password' : 'f59d8784',
				'database' : 'heroku_32b8bc1a618bed9',
				'dialect': 'mysql',
				'sync': true,
				'forceSync': false,
				'seedDB': false
			},
			'development' : {
				'host' : '127.0.0.1',
				'port' : 3306,
				'user' : 'root',
				'password' : 'froot',
				'database' : 'cymedica',
				'dialect': 'mysql',
				'sync': true,
				'forceSync': false,
				'seedDB': false
			},
			'test' : {
				'host' : 'localhost',
				'port' : 3306,
				'user' : 'root',
				'password' : 'froot',
				'database' : 'cymedica',
				'dialect': 'mysql',
				'sync': false,
				'forceSync': false,
				'seedDB': false
			}
		};
	}

	function applicationConfig(){
		return {
			'production' : {
				'url' : 'https://' + host + ':' + port,
				'host' : host,
				'port' : port
			},
			'development' : {
				'url' : 'http://' + host + ':' + port,
				'host' : host,
				'port' : port
			},
			'test' : {
				'url' : 'http://' + host + ':' + port,
				'host' : host,
				'port' : port
			}
		};
	}
}();