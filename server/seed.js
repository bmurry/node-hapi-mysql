/*

   Seed the Database

*/
var Path = require('path');
var Promise = require('bluebird');
var config = require(Path.join(__dirname, 'config'));


module.exports = function(server){
    
    
    var device = server.plugins.dal.models.device;
    var deviceData = server.plugins.dal.models.deviceData;
    var patient = server.plugins.dal.models.patient;
    var provider = server.plugins.dal.models.provider;
    var webUser = server.plugins.dal.models.webUser;
  
    var deviceRepo = new server.plugins.dal.Repository(device);
    var deviceDataRepo = new server.plugins.dal.Repository(deviceData);
    var patientRepo = new server.plugins.dal.Repository(patient);
    var providerRepo = new server.plugins.dal.Repository(provider);
    var webUserRepo = new server.plugins.dal.Repository(webUser);
    
    
   
    webUserRepo.delete({}).then(function(didit){
        var auth = server.plugins.bll.auth;
        
        //Admin User
        auth.hash('admin-password').then(function(result){
            
    
            var webUser = {
                
                userName: 'admin',
                email: 'admin@admin.com',
                pass: result.hash,
                salt: result.salt,
                role: 'ADMIN'
            
            };
        
            webUserRepo.create(webUser, 0).then(function(result){
                console.log('UserID =  ', result.id);
            });
        });
        
        
        //Provider User
        auth.hash('provider-password').then(function(result){
            
    
            var webUser = {
                
                userName: 'provider',
                email: 'provider@provider.com',
                pass: result.hash,
                salt: result.salt,
                role: 'PROVIDER'
            
            };
        
            webUserRepo.create(webUser, 0).then(function(result){
                console.log('UserID =  ', result.id);
                
                providerRepo.delete({}).then(function(didit){
                    var providerUser = {
                        
                        webUserId: result.id,
                        firstName: 'Buddy',
                        lastName: 'Rich',
                        email: 'budy@rich.com',
                        phone: '1-800-BUD-RICH'
                        
                    };
                    
                    providerRepo.create(providerUser, 0).then(function(_provider){
                        console.log('providerUser ID =  ', _provider.id);
                        
                        var device = {
                                deviceType: 2,
                                deviceUID: '6654asdfads55sdP',
                                providerId: _provider.id
                        };
                        
                        deviceRepo.delete({}).then(function(didit){
                            deviceRepo.create(device, 0).then(function(_device){
                                
                                //Patient User
                                auth.hash('patient-password').then(function(result){
                                    
                            
                                    var webUser = {
                                        
                                        userName: 'patient',
                                        email: 'patient@patient.com',
                                        pass: result.hash,
                                        salt: result.salt,
                                        role: 'PATIENT'
                                    
                                    };
                                
                                    webUserRepo.create(webUser, 0).then(function(result){
                                        console.log('UserID =  ', result.id);
                                        
                                        patientRepo.delete({}).then(function(didit){
                                            var patientUser = {
                                                webUserId: result.id,
                                                firstName: 'Neil',
                                                lastName: 'Peart',
                                                email: 'neil@peart.com',
                                                phone: '1-800-555-6160',
                                                providerId: _provider.id,
                                                deviceId: _device.id
                                            };
                                            patientUser.providerId = _provider.id;
                                            patientRepo.create(patientUser, 0).then(function(_patient){
                                                console.log('patientUser ID =  ', _patient.id);
                                                
                                                var dd1 = {
                                                    deviceId: _device.id,
                                                    patientId: _patient.id,
                                                    dataType: 1,
                                                    value: 'Simple Text'
                                                };
                                                
                                                var dd2 = {
                                                    deviceId: _device.id,
                                                    patientId: _patient.id,
                                                    dataType: 2,
                                                    value: JSON.stringify({timeStamp: new Date(), angleMin: 33.543, angleMax: 142.266})
                                                };
                                                deviceDataRepo.delete({}).then(function(didit){
                                                    deviceDataRepo.create(dd1).then(function(_dd1){
                                                        console.log('deviceData1 ID =  ', _dd1.id);
                                                        deviceDataRepo.create(dd2).then(function(_dd2){
                                                            console.log('deviceData2 ID =  ', _dd2.id);
                                                        });
                                                    }); 
                                                });
                                            });
                                        });
                                    });
                                });                 
                            });           
                        });
                    });
                });
            });
        });
    });
};
