var Crypto = require("crypto"); 
var Promise = require('bluebird');

function verifyPassword(password, hash, salt){
	return new Promise(function (resolve, reject) {
			
		
		Crypto.pbkdf2( password, salt, 10000, 512, function(err, dk) { 
			var key = Buffer(dk, 'binary').toString('hex');
			if(err){
				reject({'error': err});
			}else{
				resolve(key === hash);
			}
			
		});
	});
}
function hashPassword(password){
	
	return new Promise(function (resolve, reject) {
		
		var salt = Crypto.randomBytes(128).toString('base64');
		
		Crypto.pbkdf2( password, salt, 10000, 512, function(err, dk) { 
			
			if(err){
				reject({'error': err});
			}else{
				var key = new Buffer(dk, 'binary').toString('hex');
				resolve({'hash': key, 'salt': salt});
			}
			
		});
	});
}


module.exports = {

	'verify': verifyPassword,
	'hash': hashPassword	
};