
var Path = require('path');

exports.register = function (server, options, next) {
	
	var auth = require(Path.join(__dirname,'/auth'));
	var userAuth = require(Path.join(__dirname,'/userAuth'));
	
	server.expose('auth', auth);
	server.expose('userAuth', userAuth);
	

	next();	
};


exports.register.attributes = {
    name: 'bll',
    version: '1.0.0'
};