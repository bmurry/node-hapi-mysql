var Promise = require('bluebird');
var auth = require('../auth');

function authenticateUser(password, user){
	
	return new Promise(function (resolve, reject) {
		if(password && user){
			auth.verify(password, user.pass, user.salt).then(function(result){
				resolve(result);
			}).catch(function(err){
				reject({'error': err, 'message': 'User verification failed'})
			});
		}else{
			reject({'error': 'bad password or user'})
		}
	});
}

function updatePassword(password, user){
	
	return new Promise(function (resolve, reject) {
		if(password && user){
			auth.hashPassword(password).then(function(result){
				user.pass = result.hash;
				user.salt = result.salt;
				resolve(user);
			}).catch(function(err){
				reject({'error': err, 'message': 'Update password failed'})
			});
		}else{
			reject({'error': 'bad password or user'})
		}
	});
}


module.exports = {
	'update': updatePassword,
	'verify': authenticateUser
};