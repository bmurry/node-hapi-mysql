var Path = require('path');
var Sequelize = require('sequelize');
var config = require('../../config');
var bb = require('bluebird');


exports.register = function (server, options, next) {
    
    var sequelize = null;
    var env = process.env.NODE_ENV || 'development';
    
    var dbConfig = {
        host: config.db.host,
        dialect: config.db.dialect,
        
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        } , 
        query:{
            raw:true
        }
    };
    
    if(config.db.port){
        dbConfig.port = config.db.port;
    }
    
    var sequelize = new Sequelize(config.db.database, config.db.user, config.db.password, dbConfig);
    // test the database connection
    sequelize.authenticate().then(function() {
            
            console.log('DB connection success')
            var models = require(Path.join(__dirname, '/models'))(sequelize);
            var repository = require(Path.join(__dirname, '/dao'));
 
            server.expose('db', sequelize);
            server.expose('Sequelize', Sequelize);
            server.expose('models', models);
            server.expose('Repository', repository);
            
        })
        .catch(function(err) {
            console.error('Error connecting to DB', err)
            
        }).finally(function(){
            next();
        });
   
};

exports.register.attributes = {
    name: 'dal',
    version: '1.0.0'
};