var path = require('path');
var fs = require('fs'); 
var config = require('../../../config');


module.exports = function(sequelize){
  var result = {};
  fs.readdirSync(__dirname).forEach(function (file) {
  
    if (file != 'index.js') {
       result[path.basename(file, '.js')] = sequelize.import(path.join(__dirname, file));
       if(config.db.sync){
         result[path.basename(file, '.js')].sync({force: config.db.forceSync});
       }
    }
       
  });
  return result;
};
