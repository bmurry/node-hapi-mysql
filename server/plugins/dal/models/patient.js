
var path = require('path');

module.exports = function(sequelize, DataTypes) {
 
  var deviceData = sequelize.import(path.join(__dirname, 'deviceData.js'));
  var model = sequelize.define('patient', { 
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    webUserId: {
      type: DataTypes.BIGINT,
      allowNull: true,
    },
    providerId: {
      type: DataTypes.BIGINT,
      allowNull: true,
    },
    deviceId: {
      type: DataTypes.BIGINT,
      allowNull: true,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    created: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    modified: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    modifiedBy: {
      type: DataTypes.BIGINT,
      allowNull: false,
    }
  },{timestamps: false});
  
  model.hasMany(deviceData, {as: 'events'});
  
  return model;
};
