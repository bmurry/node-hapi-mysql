var path = require('path');

module.exports = function(sequelize, DataTypes) {
  var patient = sequelize.import(path.join(__dirname, 'patient.js'));
  var provider = sequelize.import(path.join(__dirname, 'provider.js'));
  
  var model = sequelize.define('webUser', { 
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    userName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    pass: {
      type: DataTypes.STRING(2048),
      allowNull: false
    },
    salt: {
      type: DataTypes.STRING(1024),
      allowNull: false,
    },
    role: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    roleData: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    created: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    modified: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    modifiedBy: {
      type: DataTypes.BIGINT,
      allowNull: false,
    }
  },{timestamps: false});
  
  model.hasOne(patient);
  model.hasOne(provider);
  return model;
};
