var path = require('path');

module.exports = function(sequelize, DataTypes) {
  
  var model = sequelize.define('devicedata', { 
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    deviceId: {
      type: DataTypes.BIGINT,
      allowNull: true,
    },
    patientId: {
      type: DataTypes.BIGINT,
      allowNull: true,
    },
    dataType: {
      type: DataTypes.INTEGER(3),
      allowNull: false,
    },
    value: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    created: {
      type: DataTypes.DATE,
      allowNull: false,
    }
  },{timestamps: false});
  
  return model;
};
