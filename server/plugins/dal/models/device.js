var path = require('path');

module.exports = function(sequelize, DataTypes) {
  var patient = sequelize.import(path.join(__dirname, 'patient.js'));
  var deviceData = sequelize.import(path.join(__dirname, 'deviceData.js'));
  var model = sequelize.define('device', { 
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    providerId: {
      type: DataTypes.BIGINT,
      allowNull: true,
    },
    deviceType: {
      type: DataTypes.INTEGER(3),
      allowNull: false,
      defaultValue: '1'
    },
    deviceUID: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    created: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    modified: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    modifiedBy: {
      type: DataTypes.BIGINT,
      allowNull: false,
    }
  },{timestamps: false});
  
  model.hasMany(deviceData, {as: 'events'});
  model.hasMany(patient);
  
  return model;
};
