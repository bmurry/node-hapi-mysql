var path = require('path')

module.exports = function(sequelize, DataTypes) {
  
  var patient = sequelize.import(path.join(__dirname, 'patient.js'));
  var device = sequelize.import(path.join(__dirname, 'device.js'));
  var model = sequelize.define('provider', { 
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    webUserId: {
      type: DataTypes.BIGINT,
      allowNull: true,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    created: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    modified: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    modifiedBy: {
      type: DataTypes.BIGINT,
      allowNull: false,
    }
  },{timestamps: false});
  
  model.hasMany(patient);
  model.hasMany(device);
  return model
};
