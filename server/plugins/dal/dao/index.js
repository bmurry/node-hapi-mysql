
var Promise = require('bluebird');
/*

   Assumes all entities have created (dateTime), modified (dateTime) and modifiedBy (int) fields;

*/
function Repository(dao) {
	
	var _dao = dao;

	this.create = function (defaults, user) {

		return new Promise(function (resolve, reject) {
			defaults = defaults || {};

			defaults.modified = new Date();
			defaults.created = new Date();
			defaults.modifiedBy = user;

			_dao.create(defaults, { raw: true }).then(function (result) {
				resolve(result.get());
			}).catch(function (err) {
				console.error('Error creating object', err);
				reject({ "error": err, "message": 'Error creating object' });
			});
		});
	};

	this.save = function (entity, user) {

		return new Promise(function (resolve, reject) {
			entity.modified = new Date();
			entity.modifiedBy = user;

			_dao.update(
				entity,
				{
					where: { id: entity.id }
				})
				.then(function () {
					resolve(true);
				})
				.catch(function (err) {
					console.error('Error updating object', err);
					reject({ "error": err, "message": 'Error updating object' });
			});
		});
	};

	this.getPage = function (limit, offset) {

		return new Promise(function (resolve, reject) {
			limit = limit || 20;
			offset = offset || 0;
			_dao.findAll({ offset: offset, limit: limit }).then(function (result) {
				resolve(result);
			}).catch(function (err) {
				console.error('Error getting page:  offset = ' + offset + ' & limit = ' + limit, err);
				reject({ "error": err, "message": 'Error getting page:  offset = ' + offset + ' & limit = ' + limit });
			});
		});
	};

	this.getById = function (id, attributes, includes) {

		return new Promise(function (resolve, reject) {
			var q = {};

			if (attributes) {
				q.attributes = attributes;
			}
			if (includes) {
				q.includes = includes;
			}
			_dao.findById(id, q).then(function (result) {
				resolve(result);
			}).catch(function (err) {
				console.error('Error deleting object');
				reject({ "error": err, "message": 'Error deleting object' });
			});
		});
	};

	this.getOne = function (query, attributes, includes) {

		return new Promise(function (resolve, reject) {

			var q = {
				where: query
			};

			if (attributes) {
				q.attributes = attributes;
			}
			if (includes) {
				q.includes = includes;
			}

			_dao.findOne(q).then(function (result) {
				resolve(result);
			}).catch(function (err) {
				console.error('Error finding one by query');
				reject({ "error": err, "message": 'Error finding one by query' });
			});
		});
	};

	this.getMany = function (query, attributes, includes) {

		return new Promise(function (resolve, reject) {
			
			var q = {
				where: query
			};

			if (attributes) {
				q.attributes = attributes;
			}
			if (includes) {
				q.includes = includes;
			}

			_dao.findAll(q).then(function (result) {
				resolve(result);
			}).catch(function (err) {
				console.error('Error finding one by query');
				reject({ "error": err, "message": 'Error finding many by query' });
			});
		});
	};

	this.deleteById = function (id) {

		return new Promise(function (resolve, reject) {

			_dao.destroy({
				where: {
					id: id
				}
			}).then(function () {
				resolve(true);
			}).catch(function (err) {
				console.error('Error deleting object');
				reject({ "error": err, "message": 'Error deleting object' });
			});
		});
	};

	this.delete = function (query) {

		return new Promise(function (resolve, reject) {

			_dao.destroy({
				where: query
			}).then(function () {
				resolve(true);
			}).catch(function (err) {
				console.error('Error deleting object');
				reject({ "error": err, "message": 'Error deleting object' });
			});
		});
	};
};

module.exports = Repository;