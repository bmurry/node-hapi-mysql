var Joi = require('joi');
var Promise = require('bluebird');
var jwt = require('jsonwebtoken');
var config = require('../../config');

exports.register = function(server, options, next){

    server.register([
        {
            register: require('hapi-auth-cookie')
        }
    ], function(err) {
        if (err) {
            console.error('Failed to load a plugin:', err);
            throw err;
        }

        // Set our server authentication strategy
        server.auth.strategy('standard', 'cookie', {
            password: 'ifIhadanickel(5c)foreverytimeImadeAcookieSecret...', // cookie secret
            cookie: 'cyMedica-auth', // Cookie name
            isSecure: false, // required for non-https applications
            ttl: 24 * 60 * 60 * 1000 // Set session to 1 day
        });

    });

    server.auth.default({
        strategy: 'standard',
        scope: ['ADMIN']
    });

    server.route({
        method: 'POST',
        path: '/auth/login',
        config: {
            auth: false,
            validate: {
                payload: {
                    email: Joi.string().email().required(),
                    password: Joi.string().min(2).max(200).required()
                }
            },
            handler: function(request, reply) {

                getValidatedUser(request.payload.email, request.payload.password)
                    .then(function(result){

                        if (result.error) {
                            console.error('Feiled to authenticate user', result);
                            return reply({'error': 'Login Failed'}).code(401);
                        } else {
                            request.auth.session.set(result);
                			return reply(result);
                        }

                    })
                    .catch(function(err){
                        return reply({'error': err}).code(500);
                    });
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/auth/logout',
        config: {
            auth: false,
            handler: function(request, reply) {

                request.auth.session.clear();
                return reply({'message': 'Logout Successful'});

            }
        }
    });
    
    server.route({
        method: 'GET',
        path: '/auth/check',
        config: {
            auth: {
                strategy: 'standard',
                scope: ['ADMIN', 'PATIENT', 'PROVIDER']
            },
            handler: function(request, reply) {
 
                return reply({'isAuthenticated': request.auth.isAuthenticated});

            }
        }
        
    })
    
    function signToken(id) {
        return jwt.sign({ _id: id }, config.secrets.session, { expiresInMinutes: 60*5 });
    }

 

    function getValidatedUser(email, password){
        
		return new Promise(function (resolve, reject) {
            var auth = server.plugins.bll.userAuth;
            
            var webUser = server.plugins.dal.models.webUser;
            
            var webUserRepo = new server.plugins.dal.Repository(webUser);
            
            var result = {};
            
            webUserRepo.getOne({'email': email}).then(function(_webUser){
                
                auth.verify(password, _webUser).then(function(authenticated){
                    
                    if(authenticated){
                        result.webUser = {
                            
                          id: _webUser.id,
                          name: _webUser.userName,
                          email: _webUser.email,
                          role: _webUser.role,
                          roleData: _webUser.roleData
                          
                        };
                        result.scope = _webUser.role;
                        switch (_webUser.role) {
                            case 'PATIENT':
                                var patient = server.plugins.dal.models.patient;
                                var patientRepo = new server.plugins.dal.Repository(patient);
                                patientRepo.getOne({webUserID: _webUser.id}).then(function(_patient){
                                    result.patient = {
                                        
                                        id: _patient.id,
                                        providerId: _patient.providerId,
                                        deviceId: _patient.deviceId,
                                        firstName: _patient.firstName,
                                        lastName: _patient.lastName,
                                        email: _patient.email,
                                        phone: _patient.phone
                                        
                                    };
                                    resolve(result);
                                });
                                break;
                            case 'PROVIDER':
                                var provider = server.plugins.dal.models.provider;
                                var providerRepo = new server.plugins.dal.Repository(provider);
                                providerRepo.getOne({webUserID: _webUser.id}).then(function(_provider){
                                    result.provider = {
                                        
                                        id: _provider.id,
                                        firstName: _provider.firstName,
                                        lastName: _provider.lastName,
                                        email: _provider.email,
                                        phone: _provider.phone
                                        
                                    };
                                    resolve(result);
                                });
                                break;
                        
                            default:
                                resolve(result);
                                break;
                        }
                    }else{
                        reject({'error': 'Invalid credentials', 'message': 'Invalid user name and/or password'});
                    }
                    
                })
            }).catch(function(err){
                reject({'error': err, 'message': 'Unable to authenticate user'});
            });
        });

    }
    next();
}

exports.register.attributes = {
    name: 'auth',
	version: '1.0.0'
};
