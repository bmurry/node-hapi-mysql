var Path = require('path');
var Joi = require('joi');
var Promise = require('bluebird');
var jwt = require('jsonwebtoken');
var config = require('../../config');

exports.register = function(server, options, next){



  var rootPath = process.cwd();
  function resolvePath(tail){
      return Path.join(Path.join(rootPath, '/web/public'), tail);
  };

    server.route([
    
      //static js
      {
        method: 'GET'
        , path: '/js/{param*}'
        , config: { auth: false }
        , handler: {
          directory: {
            path: resolvePath('/js')
            , listing: false
            , index: false
          }
        }
      }
    
      //static templates
      , {
        method: 'GET'
        , path: '/templates/{param*}'
        , config: { auth: false }
        , handler: {
          directory: {
            path: resolvePath('/templates')
            , listing: false
            , index: false
          }
        }
      }
    
      //static imgs
      , {
        method: 'GET'
        , path: '/img/{param*}'
        , config: { auth: false }
        , handler: {
          directory: {
            path: resolvePath('./images')
            , listing: false
            , index: false
          }
        }
      }
    
      //static css
      , {
        method: 'GET'
        , path: '/css/{param*}'
        , config: { auth: false }
        , handler: {
          directory: {
            path: resolvePath('/css')
            , listing: false
            , index: false
          }
        }
      }
      //static bower components
     ,{
        method: 'GET'
        , path: '/bower_components/{param*}'
        , config: { auth: false }
        , handler: {
          directory: {
            path: resolvePath('/bower_components')
            , listing: false
            , index: false
          }
        }
      }
      //catch all
    ,{
      method: 'GET'
      , path: '/{p*}'
      , config: { auth: false }
      , handler: function(request, reply){
          reply.file(resolvePath('index.html'))
        }
      }
  ]);
  next();
}; 

exports.register.attributes = {
    name: 'Web',
	version: '1.0.0'
};