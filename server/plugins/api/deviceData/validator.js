var Joi = require('joi');

module.exports = function(server){

	var result = {};
	
	result.create = {
		payload:{
			deviceId: Joi.number().integer().required(),
			patientId: Joi.number().integer().required()
		}
	};
	
	result.save = {
		payload:{
			deviceId: Joi.number().integer().required(),
			patientId: Joi.number().integer().required()
		}
	};
	
	result.getPage = {
		params:{
			offset: Joi.number().integer().required(),
			limit: Joi.number().integer().required()
		}
	};
	
	result.getById = {
		params:{
			id: Joi.number().integer().min(1).required()
		}
	};
	
	result.getOne = {
		payload:{
			where: Joi.any().required()
		}
	};
	
	result.getMany = {
		payload:{
			where: Joi.any().required()
		}
	};
	
	result.deleteById = {
		payload:{
			id: Joi.number().integer().min(1).required()
		}
	};
	
	result.delete = {
		payload:{
			where: Joi.any().required()
		}
	};
	
	
	
	return result;
};