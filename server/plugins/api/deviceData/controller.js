
var Controller = require('../baseController');


module.exports = function(server){

    var deviceData = server.plugins.dal.models.deviceData;
    
    var repo = new server.plugins.dal.Repository(deviceData);
				
	var controller = new Controller(repo);
        
    return controller;
    
};