var Joi = require('joi');

module.exports = function(server){

	var result = {};
	
	result.create = {
		payload:{
			deviceType: Joi.number().integer().required(),
			deviceUID: Joi.number().integer().required()
		}
	};
	
	result.save = {
		payload:{
			deviceType: Joi.number().integer().required(),
			deviceUID: Joi.number().integer().required()
		}
	};
	
	result.getPage = {
		params:{
			offset: Joi.number().integer().required(),
			limit: Joi.number().integer().required()
		}
	};
	
	result.getById = {
		params:{
			id: Joi.number().integer().min(1).required()
		}
	};
	
	result.getOne = {
		payload:{
			where: Joi.any().required()
		}
	};
	
	result.getMany = {
		payload:{
			where: Joi.any().required()
		}
	};
	
	result.deleteById = {
		payload:{
			id: Joi.number().integer().min(1).required()
		}
	};
	
	result.delete = {
		payload:{
			where: Joi.any().required()
		}
	};
	
	
	
	return result;
};