var Path = require('path');
var Controller = require(Path.join(__dirname,'../baseController'));


module.exports = function(server){

    var devices = server.plugins.dal.models.device;
    
    var repo = new server.plugins.dal.Repository(devices);
				
	var controller = new Controller(repo);
        
    return controller;
    
};