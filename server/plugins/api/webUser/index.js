var Path = require('path');
var Promise = require('bluebird');
var Controller = require(Path.join(__dirname,'/controller'));
exports.register = function(server, options, next){
	
    
	var controller = new Controller(server);
	
	var validator = require(Path.join(__dirname,'/validator'))(server);
    
	server.route( controller.generateRoutes('webuser', validator));
		
	next();
}

exports.register.attributes = {
    name: 'webUserAPI',
	version: '1.0.0'
};