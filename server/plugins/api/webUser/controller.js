
var Controller = require('../baseController');


module.exports = function(server){

    var webUsers = server.plugins.dal.models.webUser;
    
    var repo = new server.plugins.dal.Repository(webUsers);
				
	var controller = new Controller(repo);
        
    return controller;
    
};