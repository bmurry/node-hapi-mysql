var Path = require('path');
var config = require('../../config');
var bb = require('bluebird');


exports.register = function(server, options, next){
    
 
   server.register([
        {
            register: require(Path.join(__dirname, 'device'))
        },
        {
            register: require(Path.join(__dirname, 'devicedata'))
        },
        {
            register: require(Path.join(__dirname, 'patient'))
        },
        {
            register: require(Path.join(__dirname, 'provider'))
        },
        {
            register: require(Path.join(__dirname, 'webuser'))
        }
    ],
	{
		routes:{
			prefix: '/api/v1'
		}
	}, 
	function (err) {
		if(err){
			console.error('Failed to load API plugins', err);
		}else{
			console.log('API plugins loaded');
		}
	});
		
	next();
}

exports.register.attributes = {
    name: 'API',
	version: '1.0.0'
};