var Promise = require('bluebird');


function ApiController(repo){

	var _repo = repo;
	var _self = this;
	
	this.create = function(req, reply){
		_repo.create(req.payload, req.auth.user.id).then(function(result){
			replyJSON(req, reply, result, 201);
		}).catch(function(error){
			replyError(req, reply, error);
		});
	};
	
	this.save = function(req, reply){
		_repo.save(req.payload, req.auth.user.id).then(function(result){
			replyJSON(req, reply, result, 200);
		}).catch(function(error){
			replyError(req, reply, error);
		});
	};
	
	this.getPage = function(req, reply){
		_repo.getPage(req.params.limit, req.params.offset).then(function(result){
			replyJSON(req, reply, result, 200);
		}).catch(function(error){
			replyError(req, reply, error);
		});
	};
	
	this.getById = function(req, reply){
		_repo.getById(req.params.id).then(function(result){
			replyJSON(req, reply, result, 200);
		}).catch(function(error){
			replyError(req, reply, error);
		});
	};
	
	this.getOne = function(req, reply){
		_repo.getOne(req.payload.where, req.payload.attibutes, req.payload.includes).then(function(result){
			replyJSON(req, reply, result, 200);
		}).catch(function(error){
			replyError(req, reply, error);
		});
	};
	
	this.getMany = function(req, reply){
		_repo.getMany(req.payload.where, req.payload.attibutes, req.payload.includes).then(function(result){
			replyJSON(req, reply, result, 200);
		}).catch(function(error){
			replyError(req, reply, error);
		});
	};
	
	this.deleteById = function(req, reply){
		_repo.deleteById(req.params.id).then(function(result){
			replyJSON(req, reply, result, 200);
		}).catch(function(error){
			replyError(req, reply, error);
		});
	};
	
	this.delete = function(req, reply){
		_repo.delete(req.payload.where, req.payload.attibutes, req.payload.includes).then(function(result){
			replyJSON(req, reply, result, 200);
		}).catch(function(error){
			replyError(req, reply, error);
		});
	};
	
	
	this.generateRoutes = function(typeName, validator){
		var result = [
			{
				method: 'POST',
				path: '/' + typeName,
				config: {
					validate: validator.create,
					handler: _self.create
					
				}
			},
			{
				method: 'PUT',
				path: '/' + typeName,
				config: {
					validate: validator.save,
					handler: _self.save
					
				}
			},
			{
				method: 'GET',
				path: '/' + typeName + '/page/{limit}/{offset}',
				config: {
					auth: {
						strategy: 'standard',
						scope: ['ADMIN', 'PATIENT', 'PROVIDER']
					},
					validate: validator.getPage,
					handler: _self.getPage
					
				}
			},
			{
				method: 'GET',
				path: '/' + typeName + '/{id}',
				config: {
					auth: {
						strategy: 'standard',
						scope: ['ADMIN', 'PATIENT', 'PROVIDER']
					},
					validate: validator.getById,
					handler: _self.getById
					
				}
			},
			{
				method: 'POST',
				path: '/' + typeName + '/single',
				config: {
					auth: {
						strategy: 'standard',
						scope: ['ADMIN', 'PATIENT', 'PROVIDER']
					},
					validate: validator.getOne,
					handler: _self.getOne
					
				}
			},
			{
				method: 'POST',
				path: '/' + typeName + '/many',
				config: {
					auth: {
						strategy: 'standard',
						scope: ['ADMIN', 'PATIENT', 'PROVIDER']
					},
					validate: validator.getMany,
					handler: _self.getMany
					
				}
			},
			{
				method: 'DELETE',
				path: '/' + typeName + '/{id}',
				config: {
					validate: validator.deleteById,
					handler: _self.deleteById
					
				}
			},
			{
				method: 'DELETE',
				path: '/' + typeName,
				config: {
					validate: validator.delete,
					handler: _self.delete
					
				}
			}
		];
		
		return result;
	};
	
	function replyJSON(req, reply, payload, code){
		var response = payload || {'message': 'no results'};
		reply(response).type('application/json').code(code);
	}
	function replyError(req, reply, error, code){
		code = code || 500;
		reply(error).type('application/json').code(code);
	}
	
	
};
 

module.exports = ApiController;
