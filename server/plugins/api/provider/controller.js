
var Controller = require('../baseController');


module.exports = function(server){

    var providers = server.plugins.dal.models.provider;
    
    var repo = new server.plugins.dal.Repository(providers);
				
	var controller = new Controller(repo);
        
    return controller;
    
};