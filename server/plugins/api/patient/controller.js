
var Controller = require('../baseController');


module.exports = function(server){

    var patients = server.plugins.dal.models.patient;
    
    var repo = new server.plugins.dal.Repository(patients);
				
	var controller = new Controller(repo);
        
    return controller;
    
};