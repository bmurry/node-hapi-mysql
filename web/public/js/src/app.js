
/**
 * Main AngularJS Web Application
 */
var app = angular.module('cyMedicaApp', [
  'ui.router',
  'LocalStorageModule',
   'ui.grid'
]);

app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', 'USER_ROLES', function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, USER_ROLES){
      
      // For any unmatched url, send to /route1
      $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
      });
      $stateProvider
      .state('home', {
            url: "/",
            templateUrl: "/templates/home.html"
        })
        .state('login', {
            url: "/login",
            templateUrl: "/templates/login.html"
        })
        .state('admin', {
            url: "/admin",
            templateUrl: "/templates/admin.html",
            data: {
              authorizedRoles: [USER_ROLES.admin]
            }
        })
        .state('patient', {
            url: "/patient",
            templateUrl: "/templates/patient.html",
            data: {
              authorizedRoles: [USER_ROLES.admin, USER_ROLES.patient]
            }
        })
        .state('provider', {
            url: "/provider",
            templateUrl: "/templates/provider.html",
            data: {
              authorizedRoles: [USER_ROLES.admin, USER_ROLES.provider]
            }
        });
          
       $urlRouterProvider.otherwise("/");
          
        $httpProvider.interceptors.push([
          '$injector',
          function ($injector) {
            return $injector.get('AuthInterceptor');
          }
        ]);
}]);

app.run(['$rootScope', 'AUTH_EVENTS', 'AuthService', '$state', '$stateParams',
 function ($rootScope, AUTH_EVENTS, AuthService, $state, $stateParams) {
  $rootScope.$stateParams = $stateParams;
  $rootScope.$state = $state;
  $rootScope.$on('$stateChangeStart', function (event, next) {
    
    if(next.data){
      var authorizedRoles = next.data.authorizedRoles;
      AuthService.checkAuthenticated().then(function(){
        
        if (!AuthService.isAuthorized(authorizedRoles)) {
          event.preventDefault();
          if (AuthService.isAuthenticated()) {
            // user is not allowed
            $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
          } else {
            // user is not logged in
            $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
          }
        }
      });
    }
    
  });
}]);

app.factory('AuthInterceptor', ['$rootScope', '$q', 'AUTH_EVENTS', function ($rootScope, $q, AUTH_EVENTS) {
  return {
    responseError: function (response) { 
      $rootScope.$broadcast({
        401: AUTH_EVENTS.notAuthenticated,
        403: AUTH_EVENTS.notAuthorized,
        419: AUTH_EVENTS.sessionTimeout,
        440: AUTH_EVENTS.sessionTimeout
      }[response.status], response);
      return $q.reject(response);
    }
  };
}])