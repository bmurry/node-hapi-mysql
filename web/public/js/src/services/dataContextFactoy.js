'use strict';

angular.module('cyMedicaApp')
  .factory('DataContextFactory', ['$http', '$q', 'DataContext', function ($http, $q, DataContext) {
   
   return{ 
     
      deviceContext: function(){
      
      var result = DataContext.build({'baseUrl': 'device'});
      
      return result;
      
    },
    deviceDataContext: function(){
      
      var result = DataContext.build({'baseUrl': 'devicedata'});
      
      return result;
       
   },
   patientContext: function(){
      
      var result = DataContext.build({'baseUrl': 'patient'});
      
      return result;
      
    },
   providerContext: function(){
      
      var result = DataContext.build({'baseUrl': 'provider'});
      
      return result;
      
    },
   webUserContext: function(){
      
      var result = DataContext.build({'baseUrl': 'webuser'});
      
      return result;
      
    }
  }
}]);