'use strict';

var app = angular.module('cyMedicaApp');


app.constant('AUTH_EVENTS', {
  loginSuccess: 'auth-login-success',
  loginFailed: 'auth-login-failed',
  logoutSuccess: 'auth-logout-success',
  sessionTimeout: 'auth-session-timeout',
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized'
})
app.constant('USER_ROLES', {
  all: '*',
  admin: 'ADMIN',
  patient: 'PATIENT',
  provider: 'PROVIDER'
});

app.factory('AuthService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {
  var authService = {};
 
  
    authService.user = {};
    
    authService.logIn = function(userName, password){
      return $q(function(resolve, reject){
        
      $http.post('/auth/login', {'email': userName, 'password': password}).
        then(function(result) {
          
          var user = result.data;
          if(!user.error){
                       
             localStorageService.set('user', user);
             authService.user = user;
             resolve(user);
          }
        }).catch(function(error) {
          console.error('Error logging in', error);
          reject({'error': error, 'message': 'Error logging in'});
        });
      });
   };
   
   authService.logOut = function(){
      return $q(function(resolve, reject){
        $http.get('/auth/logout').then(function(result){
            if(result && !result.error){
              
               authService.user = {};
               localStorageService.remove('user');
               resolve(true);
               
            }else{
              console.log(result);
               reject({'error': 'Error logging out'});
            }
          }).catch(function(error){
               console.log(error);
               reject({'error': error, 'message': 'Error logging out'});
          });
      });
   };
   
  authService.checkAuthenticated = function () {
    return $q(function(resolve, reject){
        $http.get('/auth/check').then(function(result){
            if(result && !result.error){
              
               var user = localStorageService.get('user');
               authService.user = user;
               resolve(user);
               
            }else{
              console.log(result);
               reject({'error': 'Error authenticating'});
            }
          }).catch(function(error){
               console.log(error);
               reject({'error': error, 'message': 'Error authenticating'});
          });
      });
  };
  authService.isAuthenticated = function () {
     var result = !!(authService.user && authService.user.webUser && authService.user.webUser.id);
     
     return result;
  };
  authService.isAuthorized = function (authorizedRoles) {
    if (!angular.isArray(authorizedRoles)) {
      authorizedRoles = [authorizedRoles];
    }
    return (authService.isAuthenticated() &&
      authorizedRoles.indexOf(authService.user.scope) !== -1);
  };
 
 
  return authService;
}]);
