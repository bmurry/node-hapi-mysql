angular.module('cyMedicaApp')
.service('DataContext', ['$http', '$q', function ($http, $q){
    
    function DataContext()  {
        
        var _init = false;
        var _baseUrl = null;
        
        
        this.create = function(proto){
        checkInit();
        return $q(function(resolve, reject){
            var url = buildBaseUrl();
            
                $http.post(url, proto).then(function(result){
                    if(result && !result.error){
                    
                    resolve(result.data || result);
                    
                    }else{
                    console.log(result);
                    reject({'error': 'Error creating object', 'POST url': url});
                    }
                } ).catch(function(error){
                    console.log(error);
                    reject({'error': error, 'message': 'Error creating object'});
                });
            });  
        };
        this.save = function(model){
        checkInit();
        return $q(function(resolve, reject){
            var url = buildBaseUrl();
            
                $http.put(url, model).then(function(result){
                    if(result && !result.error){
                    
                    resolve(result.data || result);
                    
                    }else{
                    console.log(result);
                    reject({'error': 'Error saving object', 'PUT url': url});
                    }
                } ).catch(function(error){
                    console.log(error);
                    reject({'error': error, 'message': 'Error saving object'});
                });
            });  
        };
        this.getPage = function(limit, offset){
        checkInit();
        return $q(function(resolve, reject){
            var url = buildBaseUrl(limit + '/' + offset);
            
                $http.get(url).then(function(result){
                    if(result && !result.error){
                    
                    resolve(result.data || result);
                    
                    }else{
                    console.log(result);
                    reject({'error': 'Error requesting page', 'GET url': url});
                    }
                } ).catch(function(error){
                    console.log(error);
                    reject({'error': error, 'message': 'Error requesting page'});
                });
            });  
        };
        this.getById = function(id){
        checkInit();
        return $q(function(resolve, reject){
            var url = buildBaseUrl(id);
            
                $http.get(url).then(function(result){
                    if(result && !result.error){
                    
                    resolve(result.data || result);
                    
                    }else{
                    console.log(result);
                    reject({'error': 'Error requesting by id', 'GET url': url});
                    }
                } ).catch(function(error){
                    console.log(error);
                    reject({'error': error, 'message': 'Error requesting by id'});
                });
            });  
        };
        this.getOne = function(query){
        checkInit();
        return $q(function(resolve, reject){
            var url = buildBaseUrl('single');
            
                $http.post(url, query).then(function(result){
                    if(result && !result.error){
                    
                    resolve(result.data || result);
                    
                    }else{
                    console.log(result);
                    reject({'error': 'Error requesting one by query', 'POST url': url});
                    }
                } ).catch(function(error){
                    console.log(error);
                    reject({'error': error, 'message': 'Error requesting one by query'});
                });
            });  
        };
        this.getMany = function(query){
        checkInit();
        return $q(function(resolve, reject){
            var url = buildBaseUrl('many');
            
                $http.post(url, query).then(function(result){
                    if(result && !result.error){
                    
                    resolve(result.data || result);
                    
                    }else{
                    console.log(result);
                    reject({'error': 'Error requesting many by query', 'POST url': url});
                    }
                } ).catch(function(error){
                    console.log(error);
                    reject({'error': error, 'message': 'Error requesting many by query'});
                });
            });  
        };
        this.deleteById = function(id){
        checkInit();
        return $q(function(resolve, reject){
            var url = buildBaseUrl(id);
            
                $http.delete(url).then(function(result){
                    if(result && !result.error){
                    
                    resolve(result.data || result);
                    
                    }else{
                    console.log(result);
                    reject({'error': 'Error deleting by id', 'DELETE url': url});
                    }
                } ).catch(function(error){
                    console.log(error);
                    reject({'error': error, 'message': 'Error deleting by id'});
                });
            });  
        };
        this.deleteByQuery = function(id){
        checkInit();
        return $q(function(resolve, reject){
            var url = buildBaseUrl(id);
            
                $http.delete(url).then(function(result){
                    if(result && !result.error){
                    
                    resolve(result.data || result);
                    
                    }else{
                    console.log(result);
                    reject({'error': 'Error deleting by query', 'DELETE url': url});
                    }
                }).catch(function(error){
                    console.log(error);
                    reject({'error': error, 'message': 'Error deleting query id'});
                });
            });  
        };
        
        
        
        
        
        this.init = function(options){
        if(options && options.baseUrl){
            _baseUrl = options.baseUrl;
            _init = true;
        }else{
            console.error('Invalid options', 'Cound not initialize with provided options parameter');
        }
        };
        
        
        function buildBaseUrl(tail){
        var result = '/api/v1/' + _baseUrl;
        
        if(tail && tail.length > 0){
            result += result.indexOf(result.length - 1) === '/' ? tail : '/' + tail;
        }
        
        return result;
        }
        
        function checkInit(){
            
        if(!_init){
            throw {'error': 'Context has not been initialize'};
        }
        
        }
        
        this.build = function(options){
            var result = new DataContext();
            result.init(options);
            return result;
        };
    }
    
    return new DataContext();
}]);

