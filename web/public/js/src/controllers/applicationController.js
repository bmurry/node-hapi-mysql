
angular.module('cyMedicaApp').controller('ApplicationController', ['$scope', '$state', 'USER_ROLES',
 'AuthService', 'localStorageService', function ($scope, $state, USER_ROLES, AuthService, localStorageService) {
  $scope.user = {};
  $scope.errors = {};
  $scope.userRoles = USER_ROLES;
  $scope.isAuthenticated = AuthService.isAuthenticated;
  $scope.isAuthorized = AuthService.isAuthorized;
 
  $scope.setCurrentUser = function (user) {
    $scope.user = user || {};
  };
  
  $scope.login = function(form) {
      $scope.submitted = true;

      if(form.$valid) {
        AuthService.logIn($scope.user.email,  $scope.user.password)
        .then( function(result) {
          $scope.setCurrentUser(result);
          $scope.resolveUserPage();
            
        })
        .catch( function(err) {
          $scope.errors.other = err.message;
        });
      }
    };
    
  $scope.logOut = function(){
    AuthService.logOut().then(function(result){
      $scope.setCurrentUser(null);
      $state.go('login');
    });
  };
  
  $scope.resolveUserPage = function(){
    
    var scope = $scope.user.scope || '';
    switch(scope){
            
            case 'ADMIN':
              $state.go('admin');
            break;
            
            case 'PROVIDER':
              $state.go('provider');
            break;
            
            case 'PATIENT':
              $state.go('patient');
            break;
            
            default:
              $state.go('home');
            break;
          }
  };
  
  
  function init(){
    AuthService.checkAuthenticated().then(function(result){
      $scope.setCurrentUser(result);
    }).catch(function(err){
      $state.go('login');
    });
  }
  
  
  init();
}]);