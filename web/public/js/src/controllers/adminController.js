
angular.module('cyMedicaApp')
.controller('AdminController', 
['$scope', '$state', '$http', '$q', 'DataContextFactory',
 function ($scope, $state, $http, $q, DataContextFactory) {
	 
	 $scope.providers = [];
	 $scope.devices = [];
	 
	 var _deviceContext = DataContextFactory.deviceContext();
	 
	 
	 function init(){
		 _deviceContext.getMany({where:{}}).then(function(result){
			 $scope.devices = result;
			 console.log(result);
		 });
	 }
	 
	 init();
	 
 }]);