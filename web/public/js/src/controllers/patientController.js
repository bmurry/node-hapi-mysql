
angular.module('cyMedicaApp')
.controller('PatientController', 
['$scope', '$q', 'DataContextFactory', 'AuthService',
 function ($scope, $q, DataContextFactory, AuthService) {
	 
	 $scope.patient = AuthService.user.patient || {};
	 $scope.provider = {};
	 $scope.devices = [];
	 $scope.deviceData = [];
	 
	 var _deviceContext = DataContextFactory.deviceContext();
	 var _deviceDataContext = DataContextFactory.deviceDataContext();
	 var _providerContext = DataContextFactory.providerContext();
 
	 	_deviceContext.getMany({where:{'id': $scope.patient.deviceId}}).then(function(result){
			$scope.devices = result;
		});
	 	_providerContext.getOne({where:{'id': $scope.patient.provderId}}).then(function(result){
			$scope.provider = result;
		});
		
		_deviceDataContext.getMany({where:{'deviceId': $scope.patient.deviceId, 'patientId': $scope.patient.id}}).then(function(result){
			$scope.deviceData = result;
		});
 }]);