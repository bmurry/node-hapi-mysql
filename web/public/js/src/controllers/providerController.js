
angular.module('cyMedicaApp')
.controller('ProviderController', 
['$scope', '$q', 'DataContextFactory', 'AuthService',
 function ($scope, $q, DataContextFactory, AuthService) {
	 
	 $scope.provider = AuthService.user.provider || {};
	 $scope.devices = [];
	 
	 var _deviceContext = DataContextFactory.deviceContext();
 
	 	_deviceContext.getMany({where:{'providerId': $scope.provider.id}}).then(function(result){
			$scope.devices = result;
		});
 }]);